# Chef D'oeuvre

Ce projet a été généré avec [Angular CLI](https://github.com/angular/angular-cli) en version 13.0.3. pour la partie Front-end. 
Pour la partie Back-end, c'est [Spring](https://github.com/spring-projects/spring-boot) qui a été choisi.

## Contexte

Le projet est né d’une situation concrète pendant ma formation. Il y a de plus en plus de formations en alternance dans le développement web, et il n’est pas simple, malgré le nombre toujours plus important d’offres d’emploi dans le domaine, de trouver une entreprise recherchant des alternants.

## Objectif
L’application vise à mettre en relation de futurs alternants dans le développement web avec des entreprises du numérique afin de simplifier les recherches et d’accélérer les processus de recrutement.


