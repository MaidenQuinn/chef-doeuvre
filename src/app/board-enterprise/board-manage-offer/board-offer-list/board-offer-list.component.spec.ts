import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardOfferListComponent } from './board-offer-list.component';

describe('BoardOfferListComponent', () => {
  let component: BoardOfferListComponent;
  let fixture: ComponentFixture<BoardOfferListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardOfferListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardOfferListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
