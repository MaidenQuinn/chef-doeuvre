import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardOfferItemComponent } from './board-offer-item.component';

describe('BoardOfferItemComponent', () => {
  let component: BoardOfferItemComponent;
  let fixture: ComponentFixture<BoardOfferItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardOfferItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardOfferItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
