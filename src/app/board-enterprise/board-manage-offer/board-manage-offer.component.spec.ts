import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardManageOfferComponent } from './board-manage-offer.component';

describe('BoardManageOfferComponent', () => {
  let component: BoardManageOfferComponent;
  let fixture: ComponentFixture<BoardManageOfferComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardManageOfferComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardManageOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
