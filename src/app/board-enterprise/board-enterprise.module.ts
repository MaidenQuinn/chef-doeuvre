import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BoardAccountComponent } from './board-account/board-account.component';
import { BoardAddOfferComponent } from './board-add-offer/board-add-offer.component';
import { BoardEnterpriseComponent } from './board-enterprise.component';
import { BoardFormOfferComponent } from './board-form-offer/board-form-offer.component';
import { BoardManageOfferComponent } from './board-manage-offer/board-manage-offer.component';
import { BoardOfferItemComponent } from './board-manage-offer/board-offer-item/board-offer-item.component';
import { BoardOfferListComponent } from './board-manage-offer/board-offer-list/board-offer-list.component';
import { BoardMenuComponent } from './board-menu/board-menu.component';
import { BoardProfileComponent } from './board-profile/board-profile.component';

@NgModule({
  declarations: [
    BoardAccountComponent,
    BoardAddOfferComponent,
    BoardFormOfferComponent,
    BoardManageOfferComponent,
    BoardOfferItemComponent,
    BoardOfferListComponent,
    BoardMenuComponent,
    BoardProfileComponent,
    BoardEnterpriseComponent

  ],
  imports: [
    CommonModule,
  ],
  providers: [  ]
})
export class BoardEnterpriseModule { }
