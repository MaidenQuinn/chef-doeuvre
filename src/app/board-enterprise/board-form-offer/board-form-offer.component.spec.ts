import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardFormOfferComponent } from './board-form-offer.component';

describe('BoardFormOfferComponent', () => {
  let component: BoardFormOfferComponent;
  let fixture: ComponentFixture<BoardFormOfferComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardFormOfferComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardFormOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
