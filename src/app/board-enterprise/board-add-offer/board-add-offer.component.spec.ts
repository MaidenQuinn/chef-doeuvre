import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardAddOfferComponent } from './board-add-offer.component';

describe('BoardAddOfferComponent', () => {
  let component: BoardAddOfferComponent;
  let fixture: ComponentFixture<BoardAddOfferComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardAddOfferComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardAddOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
