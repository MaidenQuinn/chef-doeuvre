import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardEnterpriseComponent } from './board-enterprise.component';

describe('BoardEnterpriseComponent', () => {
  let component: BoardEnterpriseComponent;
  let fixture: ComponentFixture<BoardEnterpriseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardEnterpriseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardEnterpriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
