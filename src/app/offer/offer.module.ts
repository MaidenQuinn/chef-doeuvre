import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { OfferDetailsComponent } from './offer-details/offer-details.component';
import { OfferKeywordsItemComponent } from './offer-keywords/offer-keywords-item/offer-keywords-item.component';
import { OfferKeywordsListComponent } from './offer-keywords/offer-keywords-list/offer-keywords-list.component';
import { OfferKeywordsComponent } from './offer-keywords/offer-keywords.component';
import { OfferListComponent } from './offer-list/offer-list.component';
import { OfferComponent } from './offer.component';

@NgModule({
  declarations: [
    OfferComponent,
    OfferListComponent,
    OfferDetailsComponent,
    OfferKeywordsComponent,
    OfferKeywordsListComponent,
    OfferKeywordsItemComponent
  ],
  imports: [
    CommonModule,
  ],
  providers: [  ]
})
export class OfferModule { }
