import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferKeywordsItemComponent } from './offer-keywords-item.component';

describe('OfferKeywordsItemComponent', () => {
  let component: OfferKeywordsItemComponent;
  let fixture: ComponentFixture<OfferKeywordsItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OfferKeywordsItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferKeywordsItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
