import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferKeywordsListComponent } from './offer-keywords-list.component';

describe('OfferKeywordsListComponent', () => {
  let component: OfferKeywordsListComponent;
  let fixture: ComponentFixture<OfferKeywordsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OfferKeywordsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferKeywordsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
