import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferKeywordsComponent } from './offer-keywords.component';

describe('OfferKeywordsComponent', () => {
  let component: OfferKeywordsComponent;
  let fixture: ComponentFixture<OfferKeywordsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OfferKeywordsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferKeywordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
