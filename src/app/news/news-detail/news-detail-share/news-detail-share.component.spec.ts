import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsDetailShareComponent } from './news-detail-share.component';

describe('NewsDetailShareComponent', () => {
  let component: NewsDetailShareComponent;
  let fixture: ComponentFixture<NewsDetailShareComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsDetailShareComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsDetailShareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
