import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsDetailSimilarComponent } from './news-detail-similar.component';

describe('NewsDetailSimilarComponent', () => {
  let component: NewsDetailSimilarComponent;
  let fixture: ComponentFixture<NewsDetailSimilarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsDetailSimilarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsDetailSimilarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
