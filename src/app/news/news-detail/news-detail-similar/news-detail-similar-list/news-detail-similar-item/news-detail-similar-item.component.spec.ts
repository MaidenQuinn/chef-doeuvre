import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsDetailSimilarItemComponent } from './news-detail-similar-item.component';

describe('NewsDetailSimilarItemComponent', () => {
  let component: NewsDetailSimilarItemComponent;
  let fixture: ComponentFixture<NewsDetailSimilarItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsDetailSimilarItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsDetailSimilarItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
