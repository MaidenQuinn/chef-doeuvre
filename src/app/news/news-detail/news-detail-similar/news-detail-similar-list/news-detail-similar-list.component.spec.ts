import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsDetailSimilarListComponent } from './news-detail-similar-list.component';

describe('NewsDetailSimilarListComponent', () => {
  let component: NewsDetailSimilarListComponent;
  let fixture: ComponentFixture<NewsDetailSimilarListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsDetailSimilarListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsDetailSimilarListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
