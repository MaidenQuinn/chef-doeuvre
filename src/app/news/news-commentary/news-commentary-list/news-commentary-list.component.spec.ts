import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsCommentaryListComponent } from './news-commentary-list.component';

describe('NewsCommentaryListComponent', () => {
  let component: NewsCommentaryListComponent;
  let fixture: ComponentFixture<NewsCommentaryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsCommentaryListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsCommentaryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
