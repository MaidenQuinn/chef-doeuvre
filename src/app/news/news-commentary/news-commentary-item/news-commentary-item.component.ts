import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-news-commentary-item',
  templateUrl: './news-commentary-item.component.html',
  styleUrls: ['./news-commentary-item.component.scss']
})
export class NewsCommentaryItemComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
