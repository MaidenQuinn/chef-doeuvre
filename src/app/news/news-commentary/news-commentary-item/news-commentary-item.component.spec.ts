import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsCommentaryItemComponent } from './news-commentary-item.component';

describe('NewsCommentaryItemComponent', () => {
  let component: NewsCommentaryItemComponent;
  let fixture: ComponentFixture<NewsCommentaryItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsCommentaryItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsCommentaryItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
