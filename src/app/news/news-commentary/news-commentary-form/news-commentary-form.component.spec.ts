import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsCommentaryFormComponent } from './news-commentary-form.component';

describe('NewsCommentaryFormComponent', () => {
  let component: NewsCommentaryFormComponent;
  let fixture: ComponentFixture<NewsCommentaryFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsCommentaryFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsCommentaryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
