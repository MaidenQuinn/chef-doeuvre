import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsKeywordsListComponent } from './news-keywords-list.component';

describe('NewsKeywordsListComponent', () => {
  let component: NewsKeywordsListComponent;
  let fixture: ComponentFixture<NewsKeywordsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsKeywordsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsKeywordsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
