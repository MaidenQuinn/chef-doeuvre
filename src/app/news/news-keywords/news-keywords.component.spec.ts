import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsKeywordsComponent } from './news-keywords.component';

describe('NewsKeywordsComponent', () => {
  let component: NewsKeywordsComponent;
  let fixture: ComponentFixture<NewsKeywordsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsKeywordsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsKeywordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
