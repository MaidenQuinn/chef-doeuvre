import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsKeywordsItemComponent } from './news-keywords-item.component';

describe('NewsKeywordsItemComponent', () => {
  let component: NewsKeywordsItemComponent;
  let fixture: ComponentFixture<NewsKeywordsItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsKeywordsItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsKeywordsItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
