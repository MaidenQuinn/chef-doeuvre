import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MostPopularItemComponent } from './most-popular-item/most-popular-item.component';
import { MostPopularListComponent } from './most-popular-list/most-popular-list.component';
import { NewsCommentaryFormComponent } from './news-commentary/news-commentary-form/news-commentary-form.component';
import { NewsCommentaryItemComponent } from './news-commentary/news-commentary-item/news-commentary-item.component';
import { NewsCommentaryListComponent } from './news-commentary/news-commentary-list/news-commentary-list.component';
import { NewsCommentaryComponent } from './news-commentary/news-commentary.component';
import { NewsDetailContentComponent } from './news-detail/news-detail-content/news-detail-content.component';
import { NewsDetailShareComponent } from './news-detail/news-detail-share/news-detail-share.component';
import { NewsDetailSimilarItemComponent } from './news-detail/news-detail-similar/news-detail-similar-list/news-detail-similar-item/news-detail-similar-item.component';
import { NewsDetailSimilarListComponent } from './news-detail/news-detail-similar/news-detail-similar-list/news-detail-similar-list.component';
import { NewsDetailSimilarComponent } from './news-detail/news-detail-similar/news-detail-similar.component';
import { NewsDetailComponent } from './news-detail/news-detail.component';
import { NewsItemComponent } from './news-item/news-item.component';
import { NewsKeywordsItemComponent } from './news-keywords/news-keywords-item/news-keywords-item.component';
import { NewsKeywordsListComponent } from './news-keywords/news-keywords-list/news-keywords-list.component';
import { NewsKeywordsComponent } from './news-keywords/news-keywords.component';
import { NewsListComponent } from './news-list/news-list.component';

@NgModule({ 
  declarations: [
    MostPopularItemComponent,
    MostPopularListComponent,
    NewsCommentaryComponent,
    NewsCommentaryFormComponent,
    NewsCommentaryItemComponent,
    NewsCommentaryListComponent,
    NewsDetailComponent,
    NewsDetailContentComponent,
    NewsDetailShareComponent,
    NewsDetailSimilarComponent,
    NewsDetailSimilarItemComponent,
    NewsDetailSimilarListComponent,
    NewsItemComponent,
    NewsKeywordsComponent,
    NewsKeywordsItemComponent,
    NewsKeywordsListComponent,
    NewsListComponent
],
  imports: [
    CommonModule,
  ],
  providers: [  ]
})
export class NewsModule { }
