import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AlertComponent } from './alert/alert.component';
import { OfferItemComponent } from './offer-item/offer-item.component';

@NgModule({
  declarations: [
    AlertComponent,
    OfferItemComponent
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    AlertComponent,
    OfferItemComponent
  ],
  providers: [  ]
})
export class SharedModule { }
