import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BoardEnterpriseModule } from './board-enterprise/board-enterprise.module';
import { BoardStudentModule } from './board-student/board-student.module';
import { CoreModule } from './core/core.module';
import { HomeModule } from './home/home.module';
import { NewsModule } from './news/news.module';
import { OfferModule } from './offer/offer.module';
import { SharedModule } from './shared/shared.module';
import { ToolboxModule } from './toolbox/toolbox.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    CoreModule,
    HomeModule,
    BoardEnterpriseModule,
    BoardStudentModule,
    NewsModule,
    OfferModule,
    ToolboxModule,
    SharedModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
