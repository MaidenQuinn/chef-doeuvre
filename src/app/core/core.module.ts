import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './auth/login/login.component';
import { LoginFormComponent } from './auth/login/login-form/login-form.component';
import { LoginSocialComponent } from './auth/login/login-social/login-social.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SignupFormComponent } from './auth/signup/signup-form/signup-form.component';
import { SignupSocialComponent } from './auth/signup/signup-social/signup-social.component';
import { ResetPasswordComponent } from './auth/reset-password/reset-password.component';

@NgModule({
  declarations: [
    AuthComponent,
       LoginComponent,
       LoginFormComponent,
       LoginSocialComponent,
       SignupComponent,
       SignupFormComponent,
       SignupSocialComponent,
       ResetPasswordComponent
  ],
  imports: [
    CommonModule,
  ],
  providers: [  ]
})
export class CoreModule { }
