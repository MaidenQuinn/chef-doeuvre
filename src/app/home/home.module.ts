import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { NewsHomeComponent } from './news-home/news-home.component';
import { OfferHomeComponent } from './offer-home/offer-home.component';
import { RegisterHomeComponent } from './register-home/register-home.component';

@NgModule({
  declarations: [
    HomeComponent,
    RegisterHomeComponent,
    NewsHomeComponent,
    OfferHomeComponent
  ],
  imports: [
    CommonModule,
  ],
  providers: [  ]
})
export class HomeModule { }
