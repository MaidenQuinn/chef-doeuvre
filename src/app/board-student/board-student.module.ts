import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BoardAccountComponent } from './board-account/board-account.component';
import { BoardApplicationItemComponent } from './board-application/board-application-list/board-application-item/board-application-item.component';
import { BoardApplicationListComponent } from './board-application/board-application-list/board-application-list.component';
import { BoardApplicationComponent } from './board-application/board-application.component';
import { BoardMenuComponent } from './board-menu/board-menu.component';
import { BoardProfileComponent } from './board-profile/board-profile.component';
import { BoardStudentComponent } from './board-student.component';

@NgModule({
  declarations: [
    BoardStudentComponent,
    BoardMenuComponent,
    BoardProfileComponent,
    BoardAccountComponent,
    BoardApplicationComponent,
    BoardApplicationListComponent,
    BoardApplicationItemComponent,

  ],
  imports: [
    CommonModule,
  ],
  providers: [  ]
})
export class BoardStudentModule { }
