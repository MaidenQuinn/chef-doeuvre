import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardApplicationComponent } from './board-application.component';

describe('BoardApplicationComponent', () => {
  let component: BoardApplicationComponent;
  let fixture: ComponentFixture<BoardApplicationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardApplicationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
