import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardApplicationListComponent } from './board-application-list.component';

describe('BoardApplicationListComponent', () => {
  let component: BoardApplicationListComponent;
  let fixture: ComponentFixture<BoardApplicationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardApplicationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardApplicationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
