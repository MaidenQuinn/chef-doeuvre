import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardApplicationItemComponent } from './board-application-item.component';

describe('BoardApplicationItemComponent', () => {
  let component: BoardApplicationItemComponent;
  let fixture: ComponentFixture<BoardApplicationItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardApplicationItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardApplicationItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
