import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardAccountComponent } from './board-account.component';

describe('BoardAccountComponent', () => {
  let component: BoardAccountComponent;
  let fixture: ComponentFixture<BoardAccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardAccountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
